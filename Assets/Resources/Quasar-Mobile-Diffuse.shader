// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Quasar/Mobile Diffuse" {

// Properties block. To tune in the Inspector tab (Table 1 in Manual)
Properties {
	// Variable_Name ("Inspector Gui Name", Type) = "Default Value"
    _MainTex ("Base (RGB)",	2D) = "white" {}
    [PerRendererData]_MainTint("Color", Color) = (1,1,1,1)
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert exclude_path:prepass noforwardadd halfasview /*nolightmap*/

sampler2D _MainTex; // The same name as in Properties block (to link them)
fixed4 _MainTint;

struct Input {
    half2 uv_MainTex; // The same name + 'uv' prefix
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
    o.Albedo = c.rgb * _MainTint;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
