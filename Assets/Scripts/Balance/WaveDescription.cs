using System;
using System.Collections.Generic;
using Assets.Scripts.Components;
using UnityEngine;

namespace Assets.Scripts.Balance
{
	[CreateAssetMenu(fileName = "Wave", menuName = "Blueprint/Wave", order = 10)]
	[Serializable]

	public class WaveDescription : ScriptableObject
	{
		public List<WaveComponent> Waves;
	}
}
