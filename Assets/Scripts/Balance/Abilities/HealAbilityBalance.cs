﻿using UnityEngine;

namespace Assets.Scripts.Balance.Abilities
{
	[CreateAssetMenu(fileName = "HealAbilityBalance", menuName = "Balance/Ability/HealAbilityBalance", order = 2)]
	public class HealAbilityBalance : AbilityBalance
	{
		public override AbilityType AbilityType => AbilityType.Heal;

		public int HealAmount;
	}
}