using UnityEngine;

namespace Assets.Scripts.Balance.Abilities
{
	public enum AbilityType
	{
		SummonMinions,
		Heal
	}

	public abstract class AbilityBalance : ScriptableObject
	{
		public float Cooldown;
		public Sprite Icon;

		public abstract AbilityType AbilityType { get; }
	}
}