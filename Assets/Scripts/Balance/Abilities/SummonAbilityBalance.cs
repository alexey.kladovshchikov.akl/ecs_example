using Assets.Scripts.Blueprints;
using UnityEngine;

namespace Assets.Scripts.Balance.Abilities
{
	[CreateAssetMenu(fileName = "SummonAbilityBalance", menuName = "Balance/Ability/SummonAbilityBalance", order = 1)]
	public class SummonAbilityBalance : AbilityBalance
	{
		public override AbilityType AbilityType => AbilityType.SummonMinions;

		public UnitBlueprint Blueprint;
		public int Count;
	}
}
