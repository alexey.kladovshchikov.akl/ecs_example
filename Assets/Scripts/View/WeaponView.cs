﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.View
{
	class WeaponView : MonoBehaviour
	{
		[SerializeField]
		private ParticleSystem _particleSystem; // Эффект выстрела

		[SerializeField]
		private Transform _origin; // Позиция из которой вылетают пули

		[SerializeField]
		private float _delay;

		public void Shot()
		{
			EmitParticles();
		}

		private void EmitParticles()
		{
			StartCoroutine(ShotCoroutine());
		}

		private IEnumerator ShotCoroutine()
		{
			yield return new WaitForSeconds(_delay);
			if (_particleSystem != null)
			{
				_particleSystem.Emit(1);
				_particleSystem.Play(true);
			}
		}

		public Vector3 GetBulletOriginPosition()
		{
			return _origin.position;
		}
    
	}
}
