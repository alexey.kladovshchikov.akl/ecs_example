using UnityEngine;

namespace Assets.Scripts.View
{
	public class DeathBehaviour : StateMachineBehaviour
	{
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			animator.GetComponentInParent<CharacterAnimation>().OnDeathAnimationFinished();
		}
	}
}
