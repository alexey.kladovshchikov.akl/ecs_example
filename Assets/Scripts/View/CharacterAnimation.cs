﻿using Assets.Scripts.Components;
using Leopotam.Ecs;
using UniRx;
using UnityEngine;

namespace Assets.Scripts.View
{
	[RequireComponent(typeof(Animator))]
	public class CharacterAnimation : MonoBehaviour, IDestroyView
	{
		private Animator _animator;
    
		private EcsEntity _entity;

		private readonly CompositeDisposable _disposable = new CompositeDisposable();

		void Start()
		{
			_animator = GetComponent<Animator>();
		}

		public void Initialize(EcsEntity entity)
		{
			_entity = entity;
			_disposable.Clear();
		}
    
		void Update()
		{
			if (_entity.Has<MovementComponent>())
			{
				_animator?.SetFloat("MovementSpeedMultiplier", _entity.Get<MovementComponent>().SpeedMultiplier);
			}
		}

		public void UpdateMovementDirection(float vertical, float horizontal)
		{
			_animator?.SetFloat("Horizontal", horizontal, 0.1f, Time.deltaTime);
			_animator?.SetFloat("Vertical", vertical, 0.1f, Time.deltaTime);
		}

		public void DoAttack()
		{
			_animator?.SetTrigger("isAttacking");
			if (_entity.IsAlive() && _entity.Has<AttackCooldown>())
			{
				_animator?.SetFloat("AttackSpeed", _entity.Get<AttackCooldown>().AttackSpeedMultiplier);
			}
		}

		public void TakeDamage()
		{
			_animator?.SetTrigger("TakeDamage");
		}

		public void OnDie()
		{
			_animator?.SetBool("isDying", true);
		}

		public void OnFire()
		{
			if (_entity.IsAlive())
			{
				_entity.Get<AnimationAttackCompleteEvent>();
			}
		}

		public void OnDeathAnimationFinished()
		{
			_animator?.SetBool("isDying", false);

			if (_entity.IsAlive())
			{
				_entity.Del<DeadAnimation>();
				_entity.Get<DeadAnimationComplete>();
				_disposable.Clear();
			}
		}
    
		public void Destroy()
		{
			_disposable.Clear();
		}
	}
}
