﻿using UnityEngine;

namespace Assets.Scripts.Common
{
	public class DontDestroyOnLoad : MonoBehaviour
	{
		void Awake()
		{
			DontDestroyOnLoad(this);
		}

	}
}
