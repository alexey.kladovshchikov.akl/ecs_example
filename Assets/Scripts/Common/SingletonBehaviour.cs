﻿using UnityEngine;

namespace Assets.Scripts.Common
{
	public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
	{
		public static T Instance { get; set; }

		[SerializeField] private bool _keepAcrossScenes = false;

		public virtual void Awake()
		{
			if (Instance)
			{
				Debug.LogWarning("Duplicate subclass of type " + typeof(T) + "! eliminating " + name + " while preserving " + Instance.name);
				Destroy(gameObject);
			}
			else
			{
				Instance = this as T;
				if (_keepAcrossScenes)
				{
					DontDestroyOnLoad(this);
				}
			}
		}

		public virtual void OnDestroy()
		{
			if (Instance == this)
			{
				Instance = null;
			}
		}
	}
}