﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Common
{
    public class EnumUtils
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static int Count<T>()
        {
            return Enum.GetValues(typeof(T)).Length;
        }

        public static T GetRandom<T>()
        {
            Array values = Enum.GetValues(typeof(T));
            Random random = new Random();
            return (T)values.GetValue(random.Next(values.Length));
        }
    }
}
