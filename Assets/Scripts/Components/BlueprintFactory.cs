﻿using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Components
{
	public class BlueprintFactory
	{
		private EcsWorld _world;

		public void SetWorld(EcsWorld world)
		{
			_world = world;
		}

		public EcsEntity Spawn(SpawnEvent spawnData)
		{
			// Create entity by the blueprint
			EcsEntity entity = spawnData.BlueprintToSpawn.CreateEntity(_world);
			Transform viewTransform = spawnData.BlueprintToSpawn.CreateView(entity, spawnData.Parent);
			viewTransform.position = spawnData.Position;
			viewTransform.rotation = spawnData.Rotation;

			entity.Get<Position>().Value = spawnData.Position;
			entity.Get<Rotation>().Value = spawnData.Rotation;

			return entity;
		}
	}
}