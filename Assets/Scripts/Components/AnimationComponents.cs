﻿using Leopotam.Ecs;

namespace Assets.Scripts.Components
{

	public struct TakeDamageAnimationEvent : IEcsIgnoreInFilter
	{
		// OneFrame
	}

	public struct AnimationAttackCompleteEvent : IEcsIgnoreInFilter
	{
		// OneFrame
	}
	
	internal struct DeadAnimation : IEcsIgnoreInFilter
	{
	}

	internal struct DeadAnimationComplete : IEcsIgnoreInFilter
	{
	}
}