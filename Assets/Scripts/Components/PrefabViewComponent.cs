using System;
using Assets.Scripts.Ui;
using Assets.Scripts.View;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Components
{
	public interface IDestroyView
	{
		public void Destroy();
	}
	
	internal struct BulletExplosionEffect
	{
		public GameObject Explosion;
	}

	[Serializable]
	public struct NavMeshAgentComponent
	{
		public NavMeshAgent Agent;

	}

	[Serializable]
	public struct CharacterAnimationComponent : IDestroyView
	{
		public CharacterAnimation Animation;

		public void Destroy()
		{
			Animation.RecycleInstance();
		}
	}

	internal struct WeaponViewComponent
	{
		public WeaponView WeaponView;
	}

	internal struct HealthViewComponent : IDestroyView
	{
		public HealthView Health;

		public void Destroy()
		{
			Health.Destroy();
		}
	}

	internal struct AbilityButtonViewComponent : IDestroyView
	{
		public SpellBattleSlot View;

		public void Destroy()
		{
			View.RecycleInstance();
		}
	}
}