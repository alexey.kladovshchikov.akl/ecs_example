﻿using System;
using Assets.Scripts.AppData;
using Assets.Scripts.Blueprints;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Components
{

	public struct BattleOnly : IEcsIgnoreInFilter
	{
		// Destroy entity after battle
	}

	public struct MakeDamageRequest
	{
		public float Damage;
	}

	public struct HealRequest
	{
		public float Heal;
	}

	public struct DamageOverTime
	{
		public float DamagePerSecond;
		public float Duration;
	}

	public struct DamageContainerEntity
	{
		// Entity that contains data for on-hit effects (damage, slow, etc.)
		public EcsEntity Entity;
	}

	internal struct HealthChangeEvent : IEcsIgnoreInFilter
	{
	}

	public struct Targetable
	{
		public Transform TargetableTransform;
	}

	public struct BattleTarget
	{
		public EcsEntity TargetEntity;
		public Transform TargetTransform;
		public float SqrDistance;
	}


	public struct InAttackRange : IEcsIgnoreInFilter
	{
	}

	public struct AttackInput : IEcsIgnoreInFilter
	{
	}

	public struct CanAttack : IEcsIgnoreInFilter
	{
		// OneFrame
	}

	public struct AttackAction : IEcsIgnoreInFilter
	{
		// OneFrame
	}


	[Serializable]
	public struct WeaponComponent
	{
		public WeaponBlueprint Weapon;
	}

	public struct SwitchWeapon
	{
		public WeaponBlueprint Weapon;
		public EcsEntity Owner;
	}

	// OneFrame
	public struct WeaponShoot
	{
		public WeaponBlueprint Weapon;

		public EcsEntity Owner;

		public Vector3 Direction;
	}

	public struct WeaponHit
	{
		public Vector3 Position;
		public EcsEntity Entity;
	}

	public struct BulletHit : IEcsIgnoreInFilter
	{
	}

	// OneFrame
	public struct ChangeGameModeEvent
	{
		public GameActiveMode NewMode;
	}

	public struct IsCompleted : IEcsIgnoreInFilter
	{
	}

}