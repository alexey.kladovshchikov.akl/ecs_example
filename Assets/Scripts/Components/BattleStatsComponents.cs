﻿using System;
using UnityEngine;

namespace Assets.Scripts.Components
{

	[Serializable]
	public struct HealthBaseComponent
	{
		public int Value;
	}

	public struct HealthCurrentComponent
	{
		public float Value;
	}


	[Serializable]
	public struct AttackRange
	{
		[SerializeField]
		private float _range;
		public float Range
		{
			get => _range;
			set { _range = value; SqrRange = _range * _range; }
		}

		public float SqrRange { get; private set; }

		public AttackRange(float range)
		{
			_range = range;
			SqrRange = _range * _range;
		}
	}

	[Serializable]
	public struct AttackDamage
	{
		public float Damage;
	}

	[Serializable]
	public struct AttackCooldown
	{
		public float Cooldown;
		public float AttackSpeedMultiplier;
	}
	
	public struct RadiusDamage
	{
		public float Radius;
	}

	public struct SlowHitEffect
	{
		public float Duration;
		public float SlowPercent;
	}
}