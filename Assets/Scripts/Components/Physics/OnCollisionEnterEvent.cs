using UnityEngine;

namespace Assets.Scripts.Components.Physics
{
	public struct OnCollisionEnterEvent
	{
		public GameObject Sender;
		public Collision Collision;
	}
}