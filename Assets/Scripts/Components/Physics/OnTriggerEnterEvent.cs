using UnityEngine;

namespace Assets.Scripts.Components.Physics
{
	public struct OnTriggerEnterEvent
	{
		public GameObject Sender;
		public Collider Collider;
	}
}