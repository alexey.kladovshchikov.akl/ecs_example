using Assets.Scripts.Components.MonoLinks.Base;
using UnityEngine;

namespace Assets.Scripts.Components.MonoLinks.MonoLinkComponents
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidBodyMonoLink : MonoLink<RigidbodyLink>
    {
#if UNITY_EDITOR
        private void OnValidate()
        {
            if (Value.Value == null)
            {
                Value = new RigidbodyLink
                {
                    Value = GetComponent<Rigidbody>()
                };
            }
        }
#endif
    }
}