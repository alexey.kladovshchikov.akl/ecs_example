using System;
using UnityEngine;

namespace Assets.Scripts.Components.MonoLinks.MonoLinkComponents
{
    [Serializable]
    public struct RigidbodyLink
    {
        public Rigidbody Value;
    }
}