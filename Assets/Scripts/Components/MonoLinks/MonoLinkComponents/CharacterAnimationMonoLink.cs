using Assets.Scripts.Components.MonoLinks.Base;
using Assets.Scripts.View;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Components.MonoLinks.MonoLinkComponents
{
	[RequireComponent(typeof(CharacterAnimation))]
	public class CharacterAnimationMonoLink : MonoLink<CharacterAnimationComponent>
	{
#if UNITY_EDITOR
		private void OnValidate()
		{
			if (Value.Animation == null)
			{
				Value = new CharacterAnimationComponent
				{
					Animation = GetComponent<CharacterAnimation>()
				};
			}
		}
#endif

		public override void Make(ref EcsEntity entity)
		{
			base.Make(ref entity);
			Value.Animation.Initialize(entity);
		}
	}
}