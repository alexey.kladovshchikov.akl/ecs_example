using Assets.Scripts.Components.MonoLinks.Base;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Components.MonoLinks.MonoLinkComponents
{
	[RequireComponent(typeof(NavMeshAgent))]
	public class NavMeshAgentMonoLink : MonoLink<NavMeshAgentComponent>
	{
#if UNITY_EDITOR
		private void OnValidate()
		{
			if (Value.Agent == null)
			{
				Value = new NavMeshAgentComponent
				{
					Agent = GetComponent<NavMeshAgent>()
				};
			}
		}
#endif
	}
}