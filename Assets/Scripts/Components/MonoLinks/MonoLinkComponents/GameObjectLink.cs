using UnityEngine;

namespace Assets.Scripts.Components.MonoLinks.MonoLinkComponents
{
    public struct GameObjectLink : IDestroyView
    {
        public GameObject Value;

        public void Destroy()
        {
	        Value.RecycleInstance();
        }
    }
}