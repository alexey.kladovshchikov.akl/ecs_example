using Assets.Scripts.Components.MonoLinks.Base;
using Leopotam.Ecs;

namespace Assets.Scripts.Components.MonoLinks.MonoLinkComponents
{
    public class GameObjectMonoLink : MonoLink<GameObjectLink>
    {
        public override void Make(ref EcsEntity entity)
        {
            entity.Get<GameObjectLink>() = new GameObjectLink
            {
                Value = gameObject
            };
        }
    }
}