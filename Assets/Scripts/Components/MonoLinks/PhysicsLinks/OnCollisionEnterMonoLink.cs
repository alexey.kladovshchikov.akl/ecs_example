using Assets.Scripts.Components.MonoLinks.Base;
using Assets.Scripts.Components.Physics;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Components.MonoLinks.PhysicsLinks
{
	public class OnCollisionEnterMonoLink : PhysicsLinkBase
	{
		public void OnCollisionEnter(Collision other)
		{
			_entity.Get<OnCollisionEnterEvent>() = new OnCollisionEnterEvent
			{
				Collision = other,
				Sender = gameObject
			};
		}
	}
}