using System;
using UnityEngine;

namespace Assets.Scripts.Components
{
	public interface ITimerComponent
	{
		public ITimer Timer { get; }
	}

	public interface ITimer
	{
		public float RemainSec { get; }
		public float Duration { get; }

		public bool IsCompleted { get; }
		public void Tick();
	}


	struct Timer : ITimer
	{
		public float Remain;
		public float Duration { get; }

		public Timer(float seconds)
		{
			Remain = seconds;
			Duration = seconds;
		}

		public void Tick()
		{
			Remain -= Time.deltaTime;
		}

		public bool IsCompleted => Remain < 0;

		public float RemainSec => Remain;
	}

	[Serializable]
	public struct WaveTimer : ITimerComponent
	{
		public ITimer Timer { get; }

		public WaveTimer(float seconds)
		{
			Timer = new Timer(seconds);
		}
	}


	[Serializable]
	public struct AbilityCooldownTimer : ITimerComponent
	{
		public ITimer Timer { get; }

		public AbilityCooldownTimer(float seconds)
		{
			Timer = new Timer(seconds);
		}
	}


	[Serializable]
	public struct AttackCooldownTimer : ITimerComponent
	{
		public ITimer Timer { get; }

		public AttackCooldownTimer(float seconds)
		{
			Timer = new Timer(seconds);
		}
	}


	public struct Slowed : ITimerComponent
	{
		public ITimer Timer { get; }
		public float Multiplier;

		public Slowed(float seconds, float percent)
		{
			Timer = new Timer(seconds);
			Multiplier = percent;
		}
	}


	public struct DeathTimer : ITimerComponent
	{
		public ITimer Timer { get; }

		public DeathTimer(float seconds)
		{
			Timer = new Timer(seconds);
		}
	}
}