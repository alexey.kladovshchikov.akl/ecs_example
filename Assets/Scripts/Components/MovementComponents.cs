﻿using System;
using UnityEngine;

namespace Assets.Scripts.Components
{

	[Serializable]
	public struct MovementComponent
	{
		public float Speed;
		[HideInInspector]
		public float SpeedMultiplier;
	}

	public struct Position
	{
		public Vector3 Value;
	}

	public struct Rotation
	{
		public Quaternion Value;
	}

	public struct MovementTarget
	{
		public Vector3 Position;
	}

	public struct MovementDirection
	{
		public Vector3 Direction;
	}
}