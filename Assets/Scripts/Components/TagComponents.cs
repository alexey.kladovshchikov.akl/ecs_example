﻿using Leopotam.Ecs;

namespace Assets.Scripts.Components
{
	public enum TeamId
	{
		Player,
		Enemy
	}

	public struct MobTag : IEcsIgnoreInFilter
	{
	}

	public struct EnemyTag : IEcsIgnoreInFilter
	{
	}

	public struct PlayerTag : IEcsIgnoreInFilter
	{
	}

	public struct PlayerTeamTag : IEcsIgnoreInFilter
	{
	}

	public struct BulletTag : IEcsIgnoreInFilter
	{
	}
}