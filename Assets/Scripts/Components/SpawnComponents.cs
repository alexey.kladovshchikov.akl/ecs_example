﻿using System;
using Assets.Scripts.Blueprints;
using UnityEngine;

namespace Assets.Scripts.Components
{

	[Serializable]
	public struct WaveComponent
	{
		public UnitBlueprint MobToSpawn; // Mob that will be spawn
		public float Amount; // Amount of mobs to spawn
		public float Interval; // Spawn mobs evenly with that interval between them  
		public float Delay; // Delay from the last end of the wave

		public WaveComponent(UnitBlueprint mobToSpawn, float amount, float interval, float delay)
		{
			MobToSpawn = mobToSpawn;
			Amount = amount;
			Interval = interval;
			Delay = delay;
		}

		public WaveComponent(WaveComponent copy)
		{
			MobToSpawn = copy.MobToSpawn;
			Amount = copy.Amount;
			Interval = copy.Interval;
			Delay = copy.Delay;
		}
	}

	// OneFrame
	public struct SpawnEvent
	{
		public Blueprint BlueprintToSpawn;
		public Vector3 Position;
		public Quaternion Rotation;
		public Transform Parent;

		public SpawnEvent(Blueprint blueprintToSpawn, Vector3 position, Quaternion rotation, Transform parent)
		{
			BlueprintToSpawn = blueprintToSpawn;
			Position = position;
			Rotation = rotation;
			Parent = parent;
		}
	}

	public struct UnitSpawnEvent
	{
		public SpawnEvent SpawnEvent;
		public TeamId Team;

		public UnitSpawnEvent(SpawnEvent spawnEvent, TeamId team)
		{
			SpawnEvent = spawnEvent;
			Team = team;
		}
	}

	public struct LifeDistance
	{
		public Vector3 StartPosition;
		public float SqrDistance { get; }
		public float Distance { get; }

		public LifeDistance(Vector3 startPosition, float distance)
		{
			StartPosition = startPosition;
			Distance = distance;
			SqrDistance = distance * distance;
		}
	}
}