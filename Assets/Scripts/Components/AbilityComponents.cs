﻿using Assets.Scripts.Balance.Abilities;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Components
{
	public interface IAbilityCommand
	{
		public void CreateEvent(EcsWorld world);
		public Sprite GetIcon();
	}

	public struct SummonAbility : IAbilityCommand
	{
		public SummonAbilityBalance AbilityBalance;

		public SummonAbility(SummonAbilityBalance abilityBalance)
		{
			AbilityBalance = abilityBalance;
        }

		public void CreateEvent(EcsWorld world)
		{
			EcsEntity entity = world.NewEntity();
			entity.Get<SummonAbility>() = this;
			entity.Get<ApplyAbility>();
		}

		public Sprite GetIcon()
		{
			return AbilityBalance.Icon;
		}
	}

	public struct PlayerHealAbility : IAbilityCommand
	{
		public HealAbilityBalance AbilityBalance;

		public PlayerHealAbility(HealAbilityBalance abilityBalance)
		{
			AbilityBalance = abilityBalance;
		}

		public void CreateEvent(EcsWorld world)
		{
			EcsEntity entity = world.NewEntity();
			entity.Get<PlayerHealAbility>() = this;
			entity.Get<ApplyAbility>();
		}

		public Sprite GetIcon()
		{
			return AbilityBalance.Icon;
		}
	}


	public struct AbilityCommandComponent
	{
		public IAbilityCommand Ability;
		public float Cooldown;

		public AbilityCommandComponent(IAbilityCommand ability, float cooldown)
		{
			Ability = ability;
			Cooldown = cooldown;
		}
	}


	public struct AbilityClickEvent : IEcsIgnoreInFilter
	{
		// OneFrame
	}

	public struct ApplyAbility : IEcsIgnoreInFilter
	{
		// OneFrame
	}
}