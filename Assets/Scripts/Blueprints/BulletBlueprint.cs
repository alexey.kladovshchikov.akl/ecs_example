﻿using System;
using Assets.Scripts.Components;
using Assets.Scripts.Components.MonoLinks.Base;
using Assets.Scripts.View;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Blueprints
{
	[CreateAssetMenu(fileName = "Bullet", menuName = "Blueprint/Bullet", order = 10)]
	[Serializable]
	public class BulletBlueprint : Blueprint
	{
		[SerializeField] private MonoEntity _prefab;
		[SerializeField] private MovementComponent _bulletMoveComponent;
	
		public override EcsEntity CreateEntity(EcsWorld world)
		{
			EcsEntity entity = world.NewEntity();
			entity.Get<BulletTag>();
			entity.Get<BattleOnly>();
			entity.Get<MovementComponent>().Speed = _bulletMoveComponent.Speed;

			return entity;
		}

		public override Transform CreateView(EcsEntity entity, Transform parent)
		{
			MonoEntity view = _prefab.GetInstance(parent.transform);
			view.Make(ref entity);

			return view.transform;
		}

	}
}