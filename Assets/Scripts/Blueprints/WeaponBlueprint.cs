﻿using System;
using Assets.Scripts.Components;
using Leopotam.Ecs;
using MyBox;
using UnityEngine;

namespace Assets.Scripts.Blueprints
{
	[CreateAssetMenu(fileName = "Weapon", menuName = "Settings/Weapon", order = 10)]
	[Serializable]
	public class WeaponBlueprint : ScriptableObject
	{
		[SerializeField] private BulletBlueprint _bulletBlueprint;
		[SerializeField] private AttackDamage _attackDamage = default;
		[SerializeField] private AttackRange _attackRange = default;
		[SerializeField] private AttackCooldown _attackCooldown = default;

		[SerializeField] private float _radiusDamage;

		[SerializeField] public bool _slow;
		[ConditionalField("_slow")]
		[SerializeField] public float _slowDuration;
		[ConditionalField("_slow")]
		[SerializeField] public float _slowPercent;

		public EcsEntity CreateDamageContainerEntity(EcsWorld world)
		{
			// DamageContainerEntity contains data for on-hit effects
			EcsEntity entity = world.NewEntity();
			entity.Get<AttackDamage>() = _attackDamage;

			if (_radiusDamage > 0)
			{
				entity.Get<RadiusDamage>().Radius = _radiusDamage;
			}

			if (_slow)
			{
				entity.Get<SlowHitEffect>().Duration = _slowDuration;
				entity.Get<SlowHitEffect>().SlowPercent = _slowPercent;
			}

			return entity;
		}

		public BulletBlueprint GetBulletBlueprint()
		{
			return _bulletBlueprint;
		}

		public bool HasBullet()
		{
			return _bulletBlueprint != null;
		}
	
		public AttackRange GetAttackRange()
		{
			return new AttackRange(_attackRange.Range);
		}

		public AttackCooldown GetAttackCooldown()
		{
			return _attackCooldown;
		}
	}
}