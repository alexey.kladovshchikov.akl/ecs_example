﻿using System;
using Assets.Scripts.Components;
using Assets.Scripts.Components.MonoLinks.Base;
using Assets.Scripts.View;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Blueprints
{
	[CreateAssetMenu(fileName = "Enemy", menuName = "Blueprint/Enemy", order = 10)]
	[Serializable]
	public class UnitBlueprint : Blueprint
	{
		[SerializeField] private MovementComponent _movementComponent = default;
		[SerializeField] private HealthBaseComponent _healthBaseComponent = default;
		[SerializeField] private MonoEntity _prefab = default;
		[SerializeField] private WeaponBlueprint _defaultWeapon = default;

		public override EcsEntity CreateEntity(EcsWorld world)
		{
			EcsEntity entity = world.NewEntity();
			entity.Get<MovementComponent>() = _movementComponent;
			entity.Get<MovementComponent>().SpeedMultiplier = 1f;
			entity.Get<HealthBaseComponent>() = _healthBaseComponent;
			entity.Get<HealthCurrentComponent>().Value = _healthBaseComponent.Value;

			entity.Get<BattleOnly>();

			world.NewEntity().Get<SwitchWeapon>() = new SwitchWeapon { Owner = entity, Weapon = _defaultWeapon };

			return entity;
		}

		public override Transform CreateView(EcsEntity entity, Transform parent)
		{
			MonoEntity view = _prefab.GetInstance(parent.transform);
			view.Make(ref entity);

			entity.Get<Targetable>().TargetableTransform = view.transform;

			WeaponView weapon = view.GetComponentInChildren<WeaponView>(); // Example how MonoBehaviour ref components can be added without MonoLink<T>
			if (weapon != null)
			{
				entity.Get<WeaponViewComponent>().WeaponView = weapon;
			}

			return view.transform;
		}

		public void SetMaxHp(float hp)
		{
			_healthBaseComponent.Value = (int)hp;
		}

		public float GetMaxHp()
		{
			return _healthBaseComponent.Value;
		}
	}
}
