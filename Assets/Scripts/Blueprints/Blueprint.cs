﻿using Leopotam.Ecs;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Blueprints
{
	public abstract class Blueprint : ScriptableObject
	{
		[SerializeField]
		protected string _id;

		public abstract EcsEntity CreateEntity(EcsWorld world);
		public abstract Transform CreateView(EcsEntity entity, Transform parent);

		public string GetId()
		{
			return _id;
		}

		protected virtual void OnValidate()
		{
#if UNITY_EDITOR
			AssetDatabase.TryGetGUIDAndLocalFileIdentifier(this, out _id, out long local);
#endif
		}
	}
}