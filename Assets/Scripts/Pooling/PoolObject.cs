﻿using System;
using System.Collections.Generic;
using UnityEngine;

interface IPoolRecycleHandler
{
	void OnPoolRecycled();
}

public static class Pool
{
	private static Transform _cacheTransform = null;
	private static readonly Dictionary<int, Stack<PoolComponent>> _pools = new Dictionary<int, Stack<PoolComponent>>();
	private static readonly Dictionary<int, RectTransform> _rectTransforms = new Dictionary<int, RectTransform>();

	public static T GetInstance<T>(this T template, Transform parent) where T : UnityEngine.Component
	{
		return template.gameObject.GetInstance(parent).GetComponent<T>();
	}

	// Get Instance from pool, Create if not exists
	public static GameObject GetInstance(this GameObject template, Transform parent)
	{
		int id = template.GetInstanceID();
		_pools.TryGetValue(id, out Stack<PoolComponent> pool);

		if (!_rectTransforms.TryGetValue(id, out RectTransform rectTransformTemplate))
		{
			rectTransformTemplate = template.GetComponent<RectTransform>();
			_rectTransforms.Add(id, rectTransformTemplate);
		}

		GameObject toReturnGameObject;

		if (pool != null)
		{
			while (pool.Count > 0)
			{
				PoolComponent p = pool.Pop();
				if (p != null)
				{
					toReturnGameObject = p.gameObject;
					toReturnGameObject.transform.SetParent(parent, false);

					if (p.rectTransform != null)
					{
						p.rectTransform = toReturnGameObject.GetComponent<RectTransform>();
						p.rectTransform.anchorMin = rectTransformTemplate.anchorMin;
						p.rectTransform.anchorMax = rectTransformTemplate.anchorMax;
						p.rectTransform.pivot = rectTransformTemplate.pivot;
						p.rectTransform.anchoredPosition = rectTransformTemplate.anchoredPosition;
						p.rectTransform.sizeDelta = rectTransformTemplate.sizeDelta;
					}
					else
					{
						toReturnGameObject.transform.localPosition = template.transform.localPosition;
					}

					toReturnGameObject.transform.localRotation = template.transform.localRotation;
					toReturnGameObject.transform.localScale = template.transform.localScale;

					p.isActive = true;
					toReturnGameObject.SetActive(true);
					return toReturnGameObject;
				}
			}
		}

		toReturnGameObject = UnityEngine.Object.Instantiate(template, parent, false);
		PoolComponent poolPoolComponent = toReturnGameObject.AddComponent<PoolComponent>();
		poolPoolComponent.id = id;
		poolPoolComponent.isActive = true;
		if (rectTransformTemplate != null)
		{
			poolPoolComponent.rectTransform = toReturnGameObject.GetComponent<RectTransform>();
		}

		poolPoolComponent.RecycleHandlers = toReturnGameObject.GetComponentsInChildren<IPoolRecycleHandler>();

#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			toReturnGameObject.hideFlags = HideFlags.DontSave;
#else
			ret.hideFlags = HideFlags.DontSave;
#endif
		}

		toReturnGameObject.gameObject.SetActive(true);
		return toReturnGameObject;
	}

	static void RecycleInstance(PoolComponent poolPoolComponent)
	{
		if (poolPoolComponent.isActive)
		{
			poolPoolComponent.isActive = false;
			int count = poolPoolComponent.RecycleHandlers.Length;
			for (int i = 0; i < count; i++)
			{
				poolPoolComponent.RecycleHandlers[i].OnPoolRecycled();
			}

			poolPoolComponent.gameObject.SetActive(false);
			if (_cacheTransform == null)
			{
				GameObject cacheObject = new GameObject("Pool");
#if UNITY_EDITOR
				if (!Application.isPlaying)
					cacheObject.hideFlags = HideFlags.DontSave;
#else
					cacheObject.hideFlags = HideFlags.DontSave;
#endif
				_cacheTransform = cacheObject.transform;
			}

			poolPoolComponent.gameObject.transform.SetParent(_cacheTransform, false);
			if (!_pools.TryGetValue(poolPoolComponent.id, out Stack<PoolComponent> pool))
			{
				pool = new Stack<PoolComponent>();
				_pools.Add(poolPoolComponent.id, pool);
			}

			pool.Push(poolPoolComponent);
		}
	}

	public static void RecycleInstance(this UnityEngine.Component obj)
	{
		if (obj == null)
		{
			return;
		}

		obj.gameObject.RecycleInstance();
	}

	public static void RecycleInstance(this GameObject obj)
	{
		if (obj == null)
		{
			return;
		}

#if UNITY_EDITOR
		if (Application.isPlaying == false)
		{
			UnityEngine.Object.DestroyImmediate(obj);
			return;
		}
#endif

		PoolComponent[] pools = obj.GetComponentsInChildren<PoolComponent>(true);
		for (int i = 0; i < pools.Length; i++)
		{
			RecycleInstance(pools[i]);
		}

		if (obj.GetComponent<PoolComponent>() == null)
		{
			UnityEngine.Object.Destroy(obj);
		}
	}

	public static void Clear()
	{
		_pools.Clear();
		_rectTransforms.Clear();
		if (_cacheTransform == null)
		{
			return;	
		}

		for (int i = _cacheTransform.childCount - 1; i >= 0; i--)
		{
			UnityEngine.Object.Destroy(_cacheTransform.GetChild(i).gameObject);
		}
	}

	class PoolComponent : MonoBehaviour
	{
		[NonSerialized]
		public int id;
		[NonSerialized]
		public bool isActive = false;
		[NonSerialized]
		public RectTransform rectTransform = null;
		[NonSerialized]
		public IPoolRecycleHandler[] RecycleHandlers = null;
	}
}