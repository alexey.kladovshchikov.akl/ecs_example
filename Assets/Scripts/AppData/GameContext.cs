﻿namespace Assets.Scripts.AppData
{
	public class GameContext
	{
		public GameActiveMode ActiveMode { get; private set; }
		public int WaveIndex { get; private set; }

		private readonly Game _game;

		public GameContext(Game game, GameActiveMode activeMode)
		{
			ActiveMode = activeMode;
			_game = game;
		}

		public void ChangeGameMode(GameActiveMode newMode)
		{
			if (newMode != ActiveMode)
			{
				_game.OnModeLeave(ActiveMode);
				ActiveMode = newMode;
				_game.OnModeEnter(newMode);
			}
		}

		public void OnWaveCompleted()
		{
			WaveIndex++;
		}
	}

	public enum GameActiveMode
	{
		Battle,
		PrepareBattle,
	}
}