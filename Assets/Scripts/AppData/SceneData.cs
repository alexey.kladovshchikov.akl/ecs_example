﻿using Assets.Scripts.Ui;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.AppData
{
    public class SceneData : MonoBehaviour
    {
	    [SerializeField] private Camera _mainCamera;
	    [SerializeField] private Gui _gui;
        [SerializeField] private Transform _playerSpawnPoint;
        [SerializeField] private Transform _battleObjectsContainer;
        [SerializeField] private BoxCollider _spawnCollider;

        public Camera MainCamera => _mainCamera;
        public Gui Gui => _gui;
        public Transform PlayerSpawnPoint => _playerSpawnPoint;
        public Transform BattleObjectsContainer => _battleObjectsContainer;
        public BoxCollider SpawnCollider => _spawnCollider;
    }
}