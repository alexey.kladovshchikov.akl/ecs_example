﻿using System.Collections.Generic;
using Assets.Scripts.Balance;
using Assets.Scripts.Balance.Abilities;
using Assets.Scripts.Blueprints;
using UnityEngine;

namespace Assets.Scripts.AppData
{
	[CreateAssetMenu(fileName = "GameConfiguration", menuName = "GameConfiguration", order = 0)]
	public class GameConfiguration : ScriptableObject
	{
		[SerializeField] private UnitBlueprint _playerBlueprint;
		[SerializeField] private List<WaveDescription> _waves; 
		[SerializeField] private List<AbilityBalance> _heroAbilities;

		public UnitBlueprint PlayerBlueprint => _playerBlueprint;
		public List<WaveDescription> Waves => _waves;
		public List<AbilityBalance> HeroAbilities => _heroAbilities;
	}
}
