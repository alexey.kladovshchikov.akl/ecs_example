﻿using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems
{
	public class MobSpawnSystem : IEcsRunSystem
	{
		private readonly EcsFilter<UnitSpawnEvent> _spawn = null;
		private readonly BlueprintFactory _blueprintFactory;

		void IEcsRunSystem.Run()
		{
			foreach (int index in _spawn)
			{
				ref UnitSpawnEvent unitSpawnRequest = ref _spawn.Get1(index);

				EcsEntity mobEntity = _blueprintFactory.Spawn(unitSpawnRequest.SpawnEvent);
				mobEntity.Get<HealthBaseComponent>().Value =
					Mathf.RoundToInt(mobEntity.Get<HealthBaseComponent>().Value);
				mobEntity.Get<HealthCurrentComponent>().Value = mobEntity.Get<HealthBaseComponent>().Value;
				mobEntity.Get<MobTag>();

				if (unitSpawnRequest.Team == TeamId.Enemy)
				{
					mobEntity.Get<EnemyTag>();
				}
				else
				{
					mobEntity.Get<PlayerTeamTag>();
				}
			}
		}
	}
}