﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems
{
	public class BattleEndSystem : IEcsRunSystem
	{
		private readonly EcsFilter<EnemyTag, HealthCurrentComponent> _aliveEnemies = null;
		private readonly EcsFilter<WaveComponent>.Exclude<IsCompleted> _notCompletedWaves = null;
		private readonly EcsFilter<UnitSpawnEvent> _spawnRequest = null;
		private readonly EcsFilter<PlayerTag>.Exclude<HealthCurrentComponent> _deadPlayer = null;
		private readonly EcsFilter<BattleOnly>.Exclude<DestroyEntityRequest> _battleOnly = null;

		private GameContext _context;
		private EcsWorld _world;

		public void Run()
		{
			if (!_deadPlayer.IsEmpty()) // player is dead
			{
				EndBattle(BattleEndType.Lose);
				return;
			}

			// No enemies - No waves to spawn
			if (_aliveEnemies.IsEmpty() && _notCompletedWaves.IsEmpty() && _spawnRequest.IsEmpty())
			{
				_context.OnWaveCompleted();
				EndBattle(BattleEndType.Win);
			}
		}

		private void EndBattle(BattleEndType endType)
		{
			foreach (int index in _battleOnly)
			{
				ref EcsEntity entity = ref _battleOnly.GetEntity(index);
				entity.Get<DestroyEntityRequest>();
			}

			_world.NewEntity().Get<ChangeGameModeEvent>().NewMode = GameActiveMode.PrepareBattle;
		}
	}

	public enum BattleEndType
	{
		Win,
		Lose
	}
}