﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Assets.Scripts.View;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.WeaponSystems
{
	public class WeaponShootSystem : IEcsRunSystem
	{
		private readonly EcsFilter<WeaponShoot> _shootEvent = null;
		private readonly BlueprintFactory _factory;
		private readonly SceneData _sceneData;
	
		public void Run()
		{
			foreach (int index in _shootEvent)
			{
				ref EcsEntity shootEntityEvent = ref _shootEvent.GetEntity(index);
				ref WeaponShoot weaponComponent = ref _shootEvent.Get1(index);

				// Create a bullet 
				ref WeaponView weaponView = ref weaponComponent.Owner.Get<WeaponViewComponent>().WeaponView;
				EcsEntity bullet = _factory.Spawn(new SpawnEvent(weaponComponent.Weapon.GetBulletBlueprint(), weaponView.GetBulletOriginPosition(),
					Quaternion.LookRotation(weaponComponent.Direction, Vector3.up), _sceneData.BattleObjectsContainer));

				// Add damage stats to bullet
				bullet.Get<DamageContainerEntity>() = shootEntityEvent.Get<DamageContainerEntity>();
				bullet.Get<MovementDirection>().Direction = weaponComponent.Direction;

				// Bullet will be destroyed after the weapon range is reached
				bullet.Get<LifeDistance>() = new LifeDistance(bullet.Get<Position>().Value, weaponComponent.Weapon.GetAttackRange().Range);

				shootEntityEvent.Get<DestroyEntityRequest>();
				weaponView.Shot();
			}
		}
	}
}