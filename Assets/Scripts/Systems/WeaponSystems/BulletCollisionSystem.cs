﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Assets.Scripts.Components.MonoLinks.Base;
using Assets.Scripts.Components.Physics;
using Leopotam.Ecs;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Assets.Scripts.Systems.WeaponSystems
{
	public class BulletCollisionSystem : IEcsRunSystem
	{
		private readonly EcsFilter<OnCollisionEnterEvent, DamageContainerEntity, BulletTag> _bullets = null;
		private SceneData _sceneData;
		private EcsWorld _world;

		public void Run()
		{
			foreach (int index in _bullets)
			{
				ref EcsEntity bulletEntity = ref _bullets.GetEntity(index);
				ref OnCollisionEnterEvent collision = ref _bullets.Get1(index);
				Vector3 hitPosition = bulletEntity.Get<Position>().Value;

				// Create WeaponHit to deal damage
				EcsEntity weaponHitEntity = _world.NewEntity();
				weaponHitEntity.Get<DamageContainerEntity>() = bulletEntity.Get<DamageContainerEntity>();
				weaponHitEntity.Get<WeaponHit>().Position = hitPosition;

				if (collision.Collision.gameObject.TryGetComponent(out MonoEntity collisionEntity))
				{
					weaponHitEntity.Get<WeaponHit>().Entity = collisionEntity.GetEntity();
				}

				if (bulletEntity.Has<BulletExplosionEffect>())
				{
					GameObject explosion = bulletEntity.Get<BulletExplosionEffect>().Explosion.GetInstance(_sceneData.BattleObjectsContainer);
					explosion.transform.position = hitPosition;
				}

				bulletEntity.Get<DestroyEntityRequest>();
			}
		}

	
	}
}