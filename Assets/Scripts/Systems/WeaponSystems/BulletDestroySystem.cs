﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.WeaponSystems
{
	public class BulletDestroySystem : IEcsRunSystem
	{
		private readonly EcsFilter<LifeDistance, BulletTag> _bullets = null;

		public void Run()
		{
			foreach (int index in _bullets)
			{
				ref EcsEntity entity = ref _bullets.GetEntity(index);
				ref LifeDistance lifeDistance = ref _bullets.Get1(index);

				if ((entity.Get<Position>().Value - lifeDistance.StartPosition).sqrMagnitude > lifeDistance.SqrDistance)
				{
					if (entity.Has<DamageContainerEntity>())
					{
						entity.Get<DamageContainerEntity>().Entity.Get<DestroyEntityRequest>();
					}

					entity.Get<DestroyEntityRequest>();
				}
			}
		}
	}
}