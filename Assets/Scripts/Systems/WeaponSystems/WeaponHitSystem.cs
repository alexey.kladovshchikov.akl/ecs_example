﻿using Assets.Scripts.Components;
using Assets.Scripts.Components.MonoLinks.Base;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.WeaponSystems
{
	public class WeaponHitSystem : IEcsRunSystem
	{
		private readonly EcsFilter<DamageContainerEntity, WeaponHit> weaponHitEvents = null;
		private EcsWorld _world;

		public void Run()
		{
			foreach (int index in weaponHitEvents) // Deal damage
			{
				ref EcsEntity weaponHitEntity = ref weaponHitEvents.GetEntity(index);
				ref EcsEntity damageContainerEntity = ref weaponHitEvents.Get1(index).Entity;
				ref WeaponHit weaponHit = ref weaponHitEvents.Get2(index);

				if (damageContainerEntity.Has<RadiusDamage>())
				{
					ApplyRadiusDamage(damageContainerEntity, weaponHit.Position, damageContainerEntity.Get<RadiusDamage>());
				}
				else
				{
					ApplyDamage(weaponHit.Entity, damageContainerEntity);
				}

				weaponHitEntity.Get<DestroyEntityRequest>();
				damageContainerEntity.Get<DestroyEntityRequest>();
			}
		}

		private void ApplyRadiusDamage(EcsEntity damageContainerEntity, Vector3 position, RadiusDamage radiusDamage)
		{
			Collider[] hitColliders = Physics.OverlapSphere(position, radiusDamage.Radius);
			foreach (Collider hitCollider in hitColliders)
			{
				MonoEntity prefabView = hitCollider.GetComponent<MonoEntity>();
				if (prefabView != null)
				{
					EcsEntity targetEntity = prefabView.GetEntity();
					ApplyDamage(targetEntity, damageContainerEntity);
				}
			}
		}

		private void ApplyDamage(EcsEntity target, EcsEntity damageContainerEntity)
		{
			if (!target.Has<HealthCurrentComponent>()) // no health - ignore
			{
				return;
			}

			AttackDamage damageComponent = damageContainerEntity.Get<AttackDamage>();

			if (target.IsAlive())
			{
				target.Get<MakeDamageRequest>().Damage += damageComponent.Damage;
				target.Get<TakeDamageAnimationEvent>();

				if (damageContainerEntity.Has<SlowHitEffect>())
				{
					float slowPercent = damageContainerEntity.Get<SlowHitEffect>().SlowPercent / 100;
					target.Get<Slowed>() = new Slowed(damageContainerEntity.Get<SlowHitEffect>().Duration, 1f - slowPercent);
				}
			}
		}
	}
}