﻿using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.WeaponSystems
{
	public class WeaponAttackSystem : IEcsRunSystem
	{
		private readonly EcsFilter<WeaponComponent, AttackAction> _attackAction = null;
		private EcsWorld _world;

		public void Run()
		{
			foreach (int index in _attackAction) // ready to action
			{
				ref EcsEntity entity = ref _attackAction.GetEntity(index);
				ref WeaponComponent weaponComponent = ref _attackAction.Get1(index);
				var damageContainerEntity = weaponComponent.Weapon.CreateDamageContainerEntity(_world);

				if (weaponComponent.Weapon.HasBullet()) // Fires projectile
				{
					EcsEntity weaponShootEntity = _world.NewEntity();
					weaponShootEntity.Get<WeaponShoot>().Weapon = entity.Get<WeaponComponent>().Weapon; // Weapon Shoot
					weaponShootEntity.Get<WeaponShoot>().Direction = entity.Get<Rotation>().Value * Vector3.forward;
					weaponShootEntity.Get<WeaponShoot>().Owner = entity;
					weaponShootEntity.Get<DamageContainerEntity>().Entity = damageContainerEntity;
				}
				else if (entity.Has<BattleTarget>()) // Instant damage to target for melee weapon
				{
					EcsEntity weaponHitEntity = _world.NewEntity();
					weaponHitEntity.Get<WeaponHit>().Position = entity.Get<BattleTarget>().TargetTransform.position;
					weaponHitEntity.Get<WeaponHit>().Entity = entity.Get<BattleTarget>().TargetEntity;
					weaponHitEntity.Get<DamageContainerEntity>().Entity = damageContainerEntity;
				}
			}
		}
	}
}