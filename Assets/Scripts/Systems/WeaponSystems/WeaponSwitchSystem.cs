﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.WeaponSystems
{
	public class WeaponSwitchSystem : IEcsRunSystem
	{
		private readonly EcsFilter<SwitchWeapon> _switchWeapon = null;

		public void Run()
		{
			foreach (int index in _switchWeapon)
			{
				ref SwitchWeapon switchWeapon = ref _switchWeapon.Get1(index);

				// Current weapon describes owner battle stats
				switchWeapon.Owner.Get<WeaponComponent>().Weapon = switchWeapon.Weapon;
				switchWeapon.Owner.Get<AttackCooldown>() = switchWeapon.Weapon.GetAttackCooldown();
				switchWeapon.Owner.Get<AttackRange>() = switchWeapon.Weapon.GetAttackRange();

				_switchWeapon.GetEntity(index).Del<SwitchWeapon>();
			}
		}
	}
}