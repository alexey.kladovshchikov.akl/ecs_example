﻿using Assets.Scripts.Components;
using Assets.Scripts.Components.MonoLinks.MonoLinkComponents;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems
{
    public class EntityDestroySystem : IEcsRunSystem
    {
        private readonly EcsFilter<CharacterAnimationComponent, DestroyEntityRequest> _characterAnimationView = null;
        private readonly EcsFilter<HealthViewComponent, DestroyEntityRequest> _healthView = null;
        private readonly EcsFilter<GameObjectLink, DestroyEntityRequest> _gameObjectView = null;
        private readonly EcsFilter<AbilityButtonViewComponent, DestroyEntityRequest> _abilityView = null;
        private readonly EcsFilter<DestroyEntityRequest> _entitiesToDestroy = null;

        void IEcsRunSystem.Run()
        {
            // Destroy / recycle view
	        DestroyView(_characterAnimationView);
	        DestroyView(_healthView);
	        DestroyView(_gameObjectView);
	        DestroyView(_abilityView);

            foreach (int i in _entitiesToDestroy)
            {
                _entitiesToDestroy.GetEntity(i).Destroy();
            }
        }

        private void DestroyView<T>(EcsFilter<T, DestroyEntityRequest> filter) where T : struct, IDestroyView  
        {
            foreach (int i in filter)
            {
	            filter.Get1(i).Destroy();
            }
        }
    }
}