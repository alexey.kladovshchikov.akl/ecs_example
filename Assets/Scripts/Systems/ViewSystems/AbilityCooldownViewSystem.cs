﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Assets.Scripts.Ui;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.ViewSystems
{
	public class AbilityCooldownViewSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
	{
		private readonly EcsFilter<AbilityButtonViewComponent, AbilityCommandComponent> _view;
		private readonly EcsFilter<AbilityCommandComponent>.Exclude<AbilityButtonViewComponent> _emptyView;
		private readonly SceneData _sceneData;

		public void Init()
		{
			foreach (int index in _emptyView)
			{
				ref EcsEntity entity = ref _emptyView.GetEntity(index);
				SpellBattleSlot view = _sceneData.Gui.SpellBattleSlot.GetInstance(_sceneData.Gui.BattleSpellContainer);
				entity.Get<AbilityButtonViewComponent>().View = view;
				view.Initialize(entity);
			}
		}

		public void Run()
		{
			foreach (int index in _view)
			{
				ref EcsEntity entity = ref _view.GetEntity(index);
				ref AbilityButtonViewComponent view = ref _view.Get1(index);
				if (entity.Has<AbilityCooldownTimer>())
				{
					ITimer timer = entity.Get<AbilityCooldownTimer>().Timer;
					view.View.UpdateTimer(timer.Duration - timer.RemainSec , timer.Duration);
				}
				else
				{
					view.View.UpdateTimer(1, 1);
				}
			}
		}

		public void Destroy()
		{
			foreach (int index in _view)
			{
				ref EcsEntity entity = ref _view.GetEntity(index);
				entity.Get<AbilityButtonViewComponent>().Destroy();
				entity.Del<AbilityButtonViewComponent>();
			}
		}
	}
}