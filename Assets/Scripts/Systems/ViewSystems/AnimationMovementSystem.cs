﻿using Assets.Scripts.Components;
using Assets.Scripts.View;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.ViewSystems
{
	public class AnimationMovementSystem : IEcsRunSystem
	{
		private readonly EcsFilter<CharacterAnimationComponent, MovementComponent> _movementAnimation = null;

		public void Run()
		{
			foreach (int index in _movementAnimation)
			{
				ref EcsEntity entity = ref _movementAnimation.GetEntity(index);
				ref CharacterAnimation animationComponent  = ref _movementAnimation.Get1(index).Animation;

				var direction = Vector3.zero;
				if (entity.Has<MovementDirection>())
				{
					direction = entity.Get<MovementDirection>().Direction;
				}
			
				if (entity.Has<NavMeshAgentComponent>() && !entity.Get<NavMeshAgentComponent>().Agent.isStopped)
				{
					direction = entity.Get<NavMeshAgentComponent>().Agent.desiredVelocity;
				}

				float vertical = Vector3.Dot(direction, entity.Get<Rotation>().Value * Vector3.forward);
				float horizontal = Vector3.Dot(direction, entity.Get<Rotation>().Value * Vector3.right);

				animationComponent.UpdateMovementDirection(vertical, horizontal);

			}
		}
	}
}