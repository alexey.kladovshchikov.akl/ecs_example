﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.ViewSystems
{
	public class AttackAnimationSystem : IEcsRunSystem
	{
		private readonly EcsFilter<CharacterAnimationComponent, CanAttack> _attackAnimation = null;
		private readonly EcsFilter<AnimationAttackCompleteEvent> _animationAttackComplete = null;

		public void Run()
		{
			foreach (int index in _attackAnimation) // Ready to Start attack animation
			{
				ref EcsEntity entity = ref _attackAnimation.GetEntity(index);
				ref CharacterAnimationComponent animation = ref _attackAnimation.Get1(index);
				animation.Animation.DoAttack();
			}

			foreach (int index in _animationAttackComplete) // Animation finished - ready to action
			{
				_animationAttackComplete.GetEntity(index).Get<AttackAction>();
			}
		}

	
	}
}