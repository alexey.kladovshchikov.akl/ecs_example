﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Assets.Scripts.Components.MonoLinks.MonoLinkComponents;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.ViewSystems
{
	public class HealthViewSystem : IEcsRunSystem
	{
		private readonly EcsFilter<HealthCurrentComponent, HealthBaseComponent, HealthChangeEvent> _healthChanged = null;
		private readonly EcsFilter<HealthBaseComponent, HealthViewComponent>.Exclude<HealthCurrentComponent> _dead = null;
		private SceneData _sceneData;

		public void Run()
		{
			foreach (int index in _healthChanged)
			{
				ref EcsEntity entity = ref _healthChanged.GetEntity(index);
				ref HealthCurrentComponent currentHealth = ref _healthChanged.Get1(index);
				ref HealthBaseComponent maxHealth = ref _healthChanged.Get2(index);

				// Create health view
				if (!entity.Has<HealthViewComponent>())
				{
					var healthView = _sceneData.Gui.UnitHealthView.GetInstance(_sceneData.Gui.WorldObjectsContainer);
					healthView.Initialize(entity.Get<GameObjectLink>().Value.transform);
					entity.Get<HealthViewComponent>().Health = healthView;
				}

				entity.Get<HealthViewComponent>().Health.UpdateValues(maxHealth.Value, currentHealth.Value);
			}

			foreach (int index in _dead)
			{
				ref EcsEntity entity = ref _dead.GetEntity(index);
				ref HealthViewComponent view = ref _dead.Get2(index);
				view.Destroy();
				entity.Del<HealthViewComponent>();
			}
		}
	}
}