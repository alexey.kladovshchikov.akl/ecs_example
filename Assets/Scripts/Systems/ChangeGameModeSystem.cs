﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems
{
	public class ChangeGameModeSystem : IEcsRunSystem
	{
		private readonly EcsFilter<ChangeGameModeEvent> _modeChanged = null;
		private GameContext _gameContext;

		public void Run()
		{
			foreach (int index in _modeChanged)
			{
				ref ChangeGameModeEvent modeEvent = ref _modeChanged.Get1(index);
				_gameContext.ChangeGameMode(modeEvent.NewMode);
			}
		}
	}
}