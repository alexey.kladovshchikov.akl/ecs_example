﻿using Assets.Scripts.AppData;
using Assets.Scripts.Balance.Abilities;
using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.PlayerSystems
{
	public class PlayerSystem : IEcsInitSystem
	{
		private readonly GameConfiguration _gameConfiguration;
		private readonly SceneData _sceneData;
		private readonly BlueprintFactory _blueprintFactory;
		private readonly EcsWorld _world;

		public void Init()
		{
			// Create player instance
			EcsEntity playerEntity = _blueprintFactory.Spawn(new SpawnEvent(_gameConfiguration.PlayerBlueprint,
				_sceneData.PlayerSpawnPoint.position, Quaternion.identity, _sceneData.PlayerSpawnPoint));

			playerEntity.Get<PlayerTag>();
			playerEntity.Get<PlayerTeamTag>();

			// Create player abilities
			foreach (AbilityBalance abilityBalance in _gameConfiguration.HeroAbilities)
			{
				CreateAbilityCommand(abilityBalance);
			}
		}

		private void CreateAbilityCommand(AbilityBalance abilityBalance)
		{
			var entity = _world.NewEntity();
			entity.Get<BattleOnly>();

			switch (abilityBalance.AbilityType)
			{
				case AbilityType.SummonMinions:
					if (abilityBalance is SummonAbilityBalance summonAbilityBalance)
					{
						SummonAbility summonAbility = new SummonAbility(summonAbilityBalance); 
						InitializeAbilityCommand(entity, summonAbility, summonAbilityBalance);
					}

					break;

				case AbilityType.Heal:
					if (abilityBalance is HealAbilityBalance healAbilityBalance)
					{
						PlayerHealAbility healAbility = new PlayerHealAbility(healAbilityBalance);
						InitializeAbilityCommand(entity, healAbility, healAbilityBalance);
					}

					break;
			}
		}

		private void InitializeAbilityCommand<T>(EcsEntity entity, T abilityComponent, AbilityBalance abilityBalance) where T: struct, IAbilityCommand
		{
			entity.Get<AbilityCommandComponent>().Ability = abilityComponent;
			entity.Get<AbilityCommandComponent>().Cooldown = abilityBalance.Cooldown;
			entity.Get<T>() = abilityComponent;
		}
	}
}