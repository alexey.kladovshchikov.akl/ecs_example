﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.PlayerSystems
{
	public class PlayerInputSystem : IEcsRunSystem, IEcsInitSystem
	{
		private readonly EcsFilter<PlayerTag> _player = null;
		private readonly SceneData _sceneData = null;

		private Plane _ground = default; // Plane to detect player mouse direction

		public void Init()
		{
			_ground = new Plane(Vector3.up, _sceneData.PlayerSpawnPoint.position);
		}

		public void Run()		
		{
			foreach (var i in _player)
			{
				ref var player = ref _player.GetEntity(i);

				// Detect player Movement direction
				var movementDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
				player.Get<MovementDirection>().Direction = movementDirection.sqrMagnitude > float.Epsilon ? movementDirection : Vector3.zero;

				// Detect player Rotation
				Ray ray = _sceneData.MainCamera.ScreenPointToRay(Input.mousePosition);
				if (_ground.Raycast(ray, out var hitDistance))
				{
					player.Get<Rotation>().Value = Quaternion.LookRotation(ray.GetPoint(hitDistance) - player.Get<Position>().Value);
				}
				
				// Detect Player Attack
				if (Input.GetKey(KeyCode.Space))
				{
					player.Get<AttackInput>();
				}
			}
        }

	}
}