﻿using Assets.Scripts.AppData;
using Assets.Scripts.Balance;
using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Systems
{
	public class WaveSystem : IEcsRunSystem, IEcsInitSystem, IEcsDestroySystem
	{
		private readonly EcsFilter<WaveComponent>.Exclude<IsCompleted, WaveTimer> _readyWave = null;
		private readonly EcsFilter<WaveComponent> _allWaves = null;

		private GameContext _gameContext;
		private GameConfiguration _configuration;
		private EcsWorld _world;
		private SceneData _sceneData;

		public void Init()
		{
			StartLevelWaves(GetWaveForLevel(_gameContext.WaveIndex));
		}

		private WaveDescription GetWaveForLevel(int waveIndex)
		{
			return _configuration.Waves[Mathf.Clamp(waveIndex, 0, _configuration.Waves.Count - 1)];
		}

		private void StartLevelWaves(WaveDescription description)
		{
			// Level stage contains several waves that spawn "Amount" of mobs with "Interval" between each other
			float waveDelay = 0;
			foreach (WaveComponent wave in description.Waves)
			{
				EcsEntity entity = _world.NewEntity();
				entity.Get<WaveComponent>().Delay = wave.Delay;
				entity.Get<WaveComponent>().Interval = wave.Interval;
				entity.Get<WaveComponent>().Amount = wave.Amount;
				entity.Get<WaveComponent>().MobToSpawn = wave.MobToSpawn;

				waveDelay += wave.Delay;
				entity.Get<WaveTimer>() = new WaveTimer(waveDelay);
				waveDelay += wave.Interval * wave.Amount;
			}
		}

		public void Run()
		{
			foreach (int index in _readyWave) // ready to spawn waves
			{
				ref EcsEntity entity = ref _readyWave.GetEntity(index);
				ref WaveComponent wave = ref _readyWave.Get1(index);

				EcsEntity eventEntity = _world.NewEntity();
				Vector3 positionToSpawn = GetRandomPointInsideCollider(_sceneData.SpawnCollider);

				eventEntity.Get<UnitSpawnEvent>().SpawnEvent = new SpawnEvent(wave.MobToSpawn, positionToSpawn,
					Quaternion.LookRotation(Vector3.left), _sceneData.SpawnCollider.transform);
				eventEntity.Get<UnitSpawnEvent>().Team = TeamId.Enemy;
				wave.Amount--;

				if (wave.Amount > 0)
				{
					entity.Get<WaveTimer>() =
						new WaveTimer(wave.Interval); // Spawn next unit of that wave after Interval
				}
				else
				{
					entity.Get<IsCompleted>();
				}
			}
		}

		public Vector3 GetRandomPointInsideCollider(BoxCollider boxCollider)
		{
			Vector3 extents = boxCollider.size / 2f;
			Vector3 point = new Vector3(
				Random.Range(-extents.x, extents.x),
				Random.Range(-extents.y, extents.y),
				Random.Range(-extents.z, extents.z)) + boxCollider.center;

			return boxCollider.transform.TransformPoint(point);
		}

		public void Destroy()
		{
			foreach (int index in _allWaves)
			{
				_allWaves.GetEntity(index).Get<DestroyEntityRequest>();
			}
		}
	}
}