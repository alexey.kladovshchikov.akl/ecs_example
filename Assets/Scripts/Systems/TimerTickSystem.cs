﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems
{
    public class TimerTickSystem : IEcsRunSystem
    {
        private readonly EcsFilter<AttackCooldownTimer> _attackCooldown = null;
        private readonly EcsFilter<Slowed> _slowed = null;
        private readonly EcsFilter<DeathTimer> _death = null;
        private readonly EcsFilter<WaveTimer> _waveTimer = null;
        private readonly EcsFilter<AbilityCooldownTimer> _abilityCooldown = null;

        void IEcsRunSystem.Run()
        {
	        TimerComponentSystem(_attackCooldown);
	        TimerComponentSystem(_slowed);
	        TimerComponentSystem(_abilityCooldown);
	        TimerComponentSystem(_waveTimer);

	        CallbackTimerSystem<DeathTimer, DestroyEntityRequest>(_death);
        }

		private void TimerComponentSystem<T>(EcsFilter<T> timerFilter) where T : struct, ITimerComponent
        {
            foreach (int i in timerFilter)
            {
                ref T component = ref timerFilter.Get1(i);
                ref EcsEntity entity = ref timerFilter.GetEntity(i);
                if (component.Timer.IsCompleted)
                {
                    entity.Del<T>();
                }
                else
                {
	                component.Timer.Tick();
                }
            }
        }	

		private void CallbackTimerSystem<T, T2>(EcsFilter<T> timerFilter) where T : struct, ITimerComponent where T2 : struct
		{
			foreach (int i in timerFilter)
			{
				ref T component = ref timerFilter.Get1(i);
				ref EcsEntity entity = ref timerFilter.GetEntity(i);
				if (component.Timer.IsCompleted)
				{
					entity.Del<T>();
					entity.Get<T2>();
				}
				else
				{
					component.Timer.Tick();
				}
			}
		}
    }
}