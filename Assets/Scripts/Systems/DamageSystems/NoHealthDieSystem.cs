﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.DamageSystems
{
	public class NoHealthDieSystem : IEcsRunSystem
	{
		private readonly EcsFilter<HealthBaseComponent>.Exclude<HealthCurrentComponent, DeadAnimation> _noAnimationFilter = null;
		private readonly EcsFilter<HealthBaseComponent, DeadAnimationComplete>.Exclude<HealthCurrentComponent, DestroyEntityRequest> _deadAnimationFilter = null;

		void IEcsRunSystem.Run()
		{
			foreach (int index in _noAnimationFilter)
			{
				ref EcsEntity entity = ref _noAnimationFilter.GetEntity(index);

				if (entity.Has<CharacterAnimationComponent>())
				{
					entity.Del<BattleTarget>();
					entity.Del<MovementTarget>();

					entity.Get<CharacterAnimationComponent>().Animation.OnDie();
					entity.Get<DeadAnimation>();
				}
				else
				{
					entity.Get<DestroyEntityRequest>();
				}
			}

			foreach (int index in _deadAnimationFilter)
			{
				ref EcsEntity entity = ref _deadAnimationFilter.GetEntity(index);
				entity.Get<DestroyEntityRequest>();
			}
		}
	}
}