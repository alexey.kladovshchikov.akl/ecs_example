﻿using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.DamageSystems
{
	public class DamageOverTimeSystem : IEcsRunSystem
	{
		private readonly EcsFilter<HealthCurrentComponent, DamageOverTime> _dots = null;

		public void Run()
		{
			foreach (int index in _dots)
			{
				ref DamageOverTime damage = ref _dots.Get2(index);
				ref EcsEntity entity = ref _dots.GetEntity(index);
				entity.Get<MakeDamageRequest>().Damage += damage.DamagePerSecond * Time.deltaTime;
            }
		}
	}
}