﻿using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.DamageSystems
{
	internal class TakeDamageSystem : IEcsRunSystem
	{
		private readonly EcsFilter<MakeDamageRequest, HealthCurrentComponent> _filter = null;

		void IEcsRunSystem.Run()
		{
			foreach (int i in _filter)
			{
				ref EcsEntity entity = ref _filter.GetEntity(i);
				ref MakeDamageRequest makeDamageRequest = ref _filter.Get1(i);
				ref HealthCurrentComponent healthCurrent = ref _filter.Get2(i);
				float healthCurrentValue = healthCurrent.Value - makeDamageRequest.Damage;

				if (healthCurrentValue > 0)
				{
					healthCurrent.Value = Mathf.Clamp(healthCurrentValue, 0, int.MaxValue);
				}
				else
				{
					entity.Del<HealthCurrentComponent>();
				}


				entity.Get<HealthChangeEvent>();

				if (entity.Has<CharacterAnimationComponent>() && entity.Has<TakeDamageAnimationEvent>())
				{
					entity.Get<CharacterAnimationComponent>().Animation.TakeDamage();
				}
			}
		}
	}
}