﻿using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.DamageSystems
{
	internal class HealSystem : IEcsRunSystem
	{
		private readonly EcsFilter<HealRequest, HealthCurrentComponent, HealthBaseComponent> _filter = null;

		void IEcsRunSystem.Run()
		{
			foreach (int i in _filter)
			{
				ref EcsEntity entity = ref _filter.GetEntity(i);
				ref HealRequest healRequest = ref _filter.Get1(i);
				ref HealthCurrentComponent healthCurrent = ref _filter.Get2(i);
				ref HealthBaseComponent healthBase = ref _filter.Get3(i);

				float healthCurrentValue = healthCurrent.Value + healRequest.Heal;
				healthCurrent.Value = Mathf.Clamp(healthCurrentValue, 0, healthBase.Value);

				entity.Get<HealthChangeEvent>();
			}
		}
	}
}