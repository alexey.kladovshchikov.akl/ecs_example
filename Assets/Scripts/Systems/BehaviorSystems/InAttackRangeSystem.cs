﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.BehaviorSystems
{
	public class InAttackRangeSystem: IEcsRunSystem
	{
		private readonly EcsFilter<BattleTarget, AttackRange, InAttackRange> _inRange = null;
		private readonly EcsFilter<BattleTarget, AttackRange>.Exclude<InAttackRange> _outOfRange = null;

		public void Run()
		{
			// Check battle target is still in range
			foreach (int index in _inRange)
			{
				ref BattleTarget target = ref _inRange.Get1(index);
				ref AttackRange attackRange = ref _inRange.Get2(index);
				ref EcsEntity entity = ref _inRange.GetEntity(index);

				if (target.SqrDistance > attackRange.SqrRange)
				{
					entity.Del<InAttackRange>();
				}
				else
				{
					entity.Del<MovementTarget>();
				}
			}

			// Check the range is reached 
			foreach (int index in _outOfRange)
			{
				ref BattleTarget target = ref _outOfRange.Get1(index);
				ref AttackRange attackRange = ref _outOfRange.Get2(index);
				ref EcsEntity entity = ref _outOfRange.GetEntity(index);

				if (target.SqrDistance <= attackRange.SqrRange)
				{
					entity.Get<InAttackRange>();
					continue;
				}

				// out of range - need to get closer
				entity.Get<MovementTarget>().Position = target.TargetTransform.position;
			}
		}
	}
}