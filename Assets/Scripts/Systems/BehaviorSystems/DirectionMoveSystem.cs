﻿using Assets.Scripts.Components;
using Assets.Scripts.Components.MonoLinks.MonoLinkComponents;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.BehaviorSystems
{
	public class DirectionMoveSystem : IEcsRunSystem
	{
		private readonly EcsFilter<MovementDirection, MovementComponent>.Exclude<NavMeshAgentComponent> _directionMovement = null;

		public void Run()
		{
			foreach (int index in _directionMovement)
			{
				var entity = _directionMovement.GetEntity(index);

				var direction = _directionMovement.Get1(index).Direction;
				ref float speed = ref _directionMovement.Get2(index).Speed;

				entity.Get<Position>().Value += direction * speed * Time.deltaTime;
				if (entity.Has<RigidbodyLink>())
				{
					entity.Get<RigidbodyLink>().Value.MovePosition(entity.Get<Position>().Value);
					entity.Get<RigidbodyLink>().Value.MoveRotation(entity.Get<Rotation>().Value);
				}
				else
				{
					entity.Get<GameObjectLink>().Value.transform.position = entity.Get<Position>().Value;
					entity.Get<GameObjectLink>().Value.transform.rotation = entity.Get<Rotation>().Value;
				}
			}
		}
	}
}