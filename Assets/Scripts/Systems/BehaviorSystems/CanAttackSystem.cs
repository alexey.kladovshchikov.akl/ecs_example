﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.BehaviorSystems
{
	public class CanAttackSystem : IEcsRunSystem
	{
		private readonly EcsFilter<BattleTarget, InAttackRange>.Exclude<AttackCooldownTimer> _mobReady = null;
		private readonly EcsFilter<PlayerTag, AttackInput>.Exclude<AttackCooldownTimer> _playerReady = null;

		public void Run()
		{
			foreach (int index in _mobReady)
			{
				_mobReady.GetEntity(index).Get<CanAttack>(); // Set unit to CanAttack State
			}

			foreach (int index in _playerReady)
			{
				_playerReady.GetEntity(index).Get<CanAttack>(); // Set player to CanAttack State
			}
		}
	}
}