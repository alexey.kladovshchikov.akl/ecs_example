﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.BehaviorSystems
{
	public class NavMeshMoveSystem : IEcsRunSystem
	{
		private readonly EcsFilter<MovementTarget, NavMeshAgentComponent, MovementComponent> _navMeshFilter = null;
		private readonly EcsFilter<NavMeshAgentComponent>.Exclude<MovementTarget> _emptyTargets = null;

		public void Run()
		{
			foreach (int index in _navMeshFilter)
			{
				ref EcsEntity entity = ref _navMeshFilter.GetEntity(index);
				ref MovementTarget movementTarget = ref _navMeshFilter.Get1(index);
				ref NavMeshAgentComponent navMeshAgent = ref _navMeshFilter.Get2(index); 
				ref MovementComponent movementSpeed = ref _navMeshFilter.Get3(index);

				// Position and Rotation controlled by NavMeshAgent
				entity.Get<Position>().Value = navMeshAgent.Agent.transform.position;
				entity.Get<Rotation>().Value = navMeshAgent.Agent.transform.rotation;

				navMeshAgent.Agent.speed = movementSpeed.Speed * movementSpeed.SpeedMultiplier;
				navMeshAgent.Agent.destination = movementTarget.Position;
				navMeshAgent.Agent.isStopped = !entity.Has<HealthCurrentComponent>();
			}

			foreach (int index in _emptyTargets)
			{
				ref NavMeshAgentComponent navMeshAgent = ref _emptyTargets.Get1(index);
				navMeshAgent.Agent.isStopped = true;
			}
		}
	}
}