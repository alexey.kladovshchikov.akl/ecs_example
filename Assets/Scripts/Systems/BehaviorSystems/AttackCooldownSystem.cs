﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.BehaviorSystems
{
	public class AttackCooldownSystem : IEcsRunSystem
	{
		private readonly EcsFilter<AttackCooldown, CanAttack> _attack = null;

		public void Run()
		{
			foreach (int index in _attack) // Ready to attack and start cooldown
			{
				ref EcsEntity entity = ref _attack.GetEntity(index);
				ref AttackCooldown attackCooldown = ref _attack.Get1(index);
				entity.Get<AttackCooldownTimer>() = new AttackCooldownTimer(attackCooldown.Cooldown);
			}
		}
	}
}