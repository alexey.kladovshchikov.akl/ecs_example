﻿using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.BehaviorSystems
{
	public class BattleTargetSystem : IEcsRunSystem
	{
		private readonly EcsFilter<MobTag>.Exclude<BattleTarget> _noTargetFiler = null;
		private readonly EcsFilter<BattleTarget> _activeTargetFilter = null;
		private readonly EcsFilter<PlayerTeamTag> _playerTeam = null;
		private readonly EcsFilter<EnemyTag> _enemyTeam = null;

		public void Run()
		{
			// Update current target
			foreach (int index in _activeTargetFilter)
			{
				ref BattleTarget target = ref _activeTargetFilter.Get1(index);
				ref EcsEntity entity = ref _activeTargetFilter.GetEntity(index);
				if (!target.TargetEntity.IsAlive() || !target.TargetEntity.Has<HealthCurrentComponent>()) // Target is dead
				{
					entity.Del<BattleTarget>();
				}
				else
				{
					UpdateTargetData(ref entity, ref target.TargetEntity);
				}
			}

			// Find a target for Mob entities
			foreach (int index in _noTargetFiler)
			{
				ref EcsEntity entity = ref _noTargetFiler.GetEntity(index);
				EcsEntity closestEntity = entity.Has<PlayerTeamTag>() ? GetRandomTarget(_enemyTeam) : GetRandomTarget(_playerTeam);

				if (closestEntity != EcsEntity.Null)
				{
					UpdateTargetData(ref entity, ref closestEntity);
				}
			}
		}

		private void UpdateTargetData(ref EcsEntity entity, ref EcsEntity targetEntity)
		{
			ref BattleTarget battleTarget = ref entity.Get<BattleTarget>();
			var position = entity.Get<Position>().Value;

			ref Transform targetTransform = ref targetEntity.Get<Targetable>().TargetableTransform;

			battleTarget.TargetEntity = targetEntity;
			battleTarget.TargetTransform = targetTransform;
			battleTarget.SqrDistance = (position - targetTransform.transform.position).sqrMagnitude;
		}

		private EcsEntity GetRandomTarget(EcsFilter filter)
		{
			if (filter.IsEmpty())
			{
				return EcsEntity.Null;
			}

			return filter.GetEntity(Random.Range(0, filter.GetEntitiesCount()));
		}
	}
}