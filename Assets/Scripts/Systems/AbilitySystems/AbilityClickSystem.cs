﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.AbilitySystems
{
	public class AbilityClickSystem : IEcsRunSystem
	{
		private readonly EcsFilter<AbilityCommandComponent, AbilityClickEvent>.Exclude<AbilityCooldownTimer> _abilityClick;
		private EcsWorld _world;

		public void Run()
		{
			foreach (int index in _abilityClick)
			{
				ref EcsEntity entity = ref _abilityClick.GetEntity(index);
				ref AbilityCommandComponent abilityCommand = ref _abilityClick.Get1(index);

				// Apply ability and start cooldown
				abilityCommand.Ability.CreateEvent(_world);
				entity.Get<AbilityCooldownTimer>() = new AbilityCooldownTimer(abilityCommand.Cooldown);
			}
		}
	}
}