﻿using Assets.Scripts.Components;
using Leopotam.Ecs;

namespace Assets.Scripts.Systems.AbilitySystems
{
	public class HealAbilitySystem : IEcsRunSystem
	{
		private readonly EcsFilter<PlayerHealAbility, ApplyAbility> _heal = null;
		private readonly EcsFilter<PlayerTag, HealthCurrentComponent> _player = null;

		public void Run()
		{
			if (_player.IsEmpty())
			{
				return;
			}

			var playerEntity = _player.GetEntity(0); // Assumes that only one player exists

			foreach (int index in _heal)
			{
				ref PlayerHealAbility healAbility = ref _heal.Get1(index);
				playerEntity.Get<HealRequest>().Heal += healAbility.AbilityBalance.HealAmount;
			}
		}
	}
}