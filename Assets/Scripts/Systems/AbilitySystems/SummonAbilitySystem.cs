﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Assets.Scripts.Systems.AbilitySystems
{
	public class SummonAbilitySystem : IEcsRunSystem
	{
		private readonly EcsFilter<SummonAbility, ApplyAbility> _summonAbility = null;
		private SceneData _sceneData;
		private EcsWorld _world;

		public void Run()
		{
			foreach (int index in _summonAbility)
			{
				ref SummonAbility summonAbility = ref _summonAbility.Get1(index);

				for (int i = 0; i < summonAbility.AbilityBalance.Count; i++)
				{
					SummonAbility ability = summonAbility;
					EcsEntity spawnRequestEntity = _world.NewEntity();

					spawnRequestEntity.Get<UnitSpawnEvent>().SpawnEvent = 
						new SpawnEvent(ability.AbilityBalance.Blueprint, _sceneData.PlayerSpawnPoint.position, Quaternion.identity, _sceneData.SpawnCollider.transform);
					spawnRequestEntity.Get<UnitSpawnEvent>().Team = TeamId.Player;
				}
			}
		}
	}
}