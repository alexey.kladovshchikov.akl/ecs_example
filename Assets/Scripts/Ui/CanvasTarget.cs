﻿using UnityEngine;

namespace Assets.Scripts.Ui
{
	class CanvasTarget : MonoBehaviour
	{
		private Vector3 _offset;
		private ITarget _target;

		public ITarget Target => _target;
		public Vector3 Offset => _offset;
		
		private void LateUpdate()
		{
			if(_target != null)
			{
				transform.localPosition = GetPosition();
			}
		}

		public Vector3 GetPosition()
		{
			Vector3 pos = Camera.main.WorldToScreenPoint(_target.GetPosition() + _offset);
			return transform.parent.worldToLocalMatrix.MultiplyPoint(pos);
		}

		public void SetTarget(ITarget target, Vector3 offset = default)
		{
			_target = target;
			_offset = offset;
			LateUpdate();
		}
	}

    public interface ITarget
    {
        Vector3 GetPosition();
    }

    public class TargetTransform : ITarget
    {
        private Transform _target;

        public TargetTransform(Transform target)
        {
            _target = target;
        }

        public Vector3 GetPosition()
        {
            return _target.position;
        }
    }
}