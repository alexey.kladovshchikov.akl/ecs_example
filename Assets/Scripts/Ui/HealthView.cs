using Assets.Scripts.Components;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui
{
	public class HealthView : MonoBehaviour, IDestroyView
	{
		[SerializeField] private CanvasGroup _canvasGroup;
		[SerializeField] private Image _progressbar;
		[SerializeField] private Image _fillImage;
		[SerializeField] private CanvasTarget _target;
		[SerializeField] private Gradient _healthGradient;

		private bool _isShown = false;

		private const float _yOffset = 4.75f;
		private const float _fillChangeDuration = 0.05f;
		private const float _canvasFadeDuration = 0.2f;
		private const float _targetAlpha = 0.75f;

		public void Initialize(Transform target)
		{
			_isShown = false;
			_canvasGroup.alpha = 0;
			_target.SetTarget(new TargetTransform(target), new Vector3(0, _yOffset, 0f));
		}

		private void Show()
		{
			_isShown = true;
			_canvasGroup.DOFade(_targetAlpha, _canvasFadeDuration).SetEase(Ease.OutSine);
		}

		public void UpdateValues(int maxHealth, float currentHealth)
		{
			if (!_isShown)
			{
				Show();
			}

			_progressbar.DOKill();
			_fillImage.color = _healthGradient.Evaluate(currentHealth / maxHealth);
			_progressbar.DOFillAmount(currentHealth / maxHealth, _fillChangeDuration).SetEase(Ease.Linear);
		}

		public void Destroy()
		{
			_progressbar.fillAmount = 0;

			_progressbar.DOFillAmount(0, _fillChangeDuration).SetEase(Ease.Linear);
			_canvasGroup.DOFade(0, _canvasFadeDuration).SetEase(Ease.OutSine).OnComplete(OnHide);
		}

		private void OnHide()
		{
			this.RecycleInstance();
		}
	}
}
