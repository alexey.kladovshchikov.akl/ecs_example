﻿using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui
{
	public class Gui : MonoBehaviour
	{
		[SerializeField] private HealthView _unitHealthView;
		[SerializeField] private SpellBattleSlot _spellBattleSlot;
		[SerializeField] private Transform _worldObjectsContainer;
		[SerializeField] private Transform _battleSpellContainer;
		[SerializeField] private Transform _prepareBattleContainer;
		[SerializeField] private Button _startBattleButton;

		public HealthView UnitHealthView => _unitHealthView;
		public SpellBattleSlot SpellBattleSlot => _spellBattleSlot;
		public Transform WorldObjectsContainer => _worldObjectsContainer;
		public Transform BattleSpellContainer => _battleSpellContainer;

		private EcsWorld _world;

		private void Awake()
		{
			_startBattleButton.onClick.RemoveAllListeners();
			_startBattleButton.onClick.AddListener(OnStartButtonClick);
		}

		public void InitializeWorld(EcsWorld world)
		{
			_world = world;
		}

		public void OnGameModeChanged(GameActiveMode activeMode)
		{
			_prepareBattleContainer.gameObject.SetActive(activeMode == GameActiveMode.PrepareBattle);
			_battleSpellContainer.gameObject.SetActive(activeMode == GameActiveMode.Battle);
		}

		private void OnStartButtonClick()
		{
			_world.NewEntity().Get<ChangeGameModeEvent>().NewMode = GameActiveMode.Battle;
		}
	}
}
