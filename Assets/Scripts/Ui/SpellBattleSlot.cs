using Assets.Scripts.Components;
using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui
{
	public class SpellBattleSlot : MonoBehaviour
	{
		[SerializeField] private Image _backgroundPanel;
		[SerializeField] private Image _spellIcon;
		[SerializeField] private Image _cooldownImage;
		[SerializeField] private Button _button;

		private EcsEntity _spellCommandEntity;
		private AbilityCommandComponent _abilityCommand;

		public void Initialize(EcsEntity entity)
		{
			_spellCommandEntity = entity;
			_abilityCommand = entity.Get<AbilityCommandComponent>();
			_button.onClick.RemoveAllListeners();
			_button.onClick.AddListener(OnClick);
			_spellIcon.sprite = _abilityCommand.Ability.GetIcon();
		}

		private void OnClick()
		{
			if (_spellCommandEntity.Has<AbilityCooldownTimer>())
			{
				return;
			}

			_spellCommandEntity.Get<AbilityClickEvent>();
		}

		public void UpdateTimer(float passed, float cooldown)
		{
			_cooldownImage.fillAmount = 1 - (passed / cooldown);
			_backgroundPanel.color = _cooldownImage.fillAmount > Mathf.Epsilon ? Color.white : Color.green;
		}
	}
}
