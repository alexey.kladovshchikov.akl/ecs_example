﻿using System.Collections.Generic;
using Assets.Scripts.AppData;
using Assets.Scripts.Components;
using Assets.Scripts.Systems;
using Assets.Scripts.Systems.AbilitySystems;
using Assets.Scripts.Systems.BehaviorSystems;
using Assets.Scripts.Systems.DamageSystems;
using Assets.Scripts.Systems.PlayerSystems;
using Assets.Scripts.Systems.ViewSystems;
using Assets.Scripts.Systems.WeaponSystems;
using Leopotam.Ecs;
using UnityEngine;

public class Game : MonoBehaviour
{
	[SerializeField] private GameConfiguration _gameConfiguration = null;
	[SerializeField] private SceneData _sceneData = null;
	[SerializeField] private GameContext _gameContext = null;

	private EcsWorld _world;
	private EcsSystems _battleUpdateSystems;
	private EcsSystems _battleFixedSystems;
	private EcsSystems _idleSystems;
	private EcsSystems _postUpdateSystems;

	private BlueprintFactory _blueprintFactory = null;

	private void Start()
	{
		_world = new EcsWorld();

		_blueprintFactory = new BlueprintFactory();
		_blueprintFactory.SetWorld(_world);

		CreateGameContext();
		CreateIdleSystems();

#if UNITY_EDITOR
		Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(_world);
#endif
	}

	private void CreateGameContext()
	{
		_gameContext = new GameContext(this, GameActiveMode.PrepareBattle);

		_sceneData.Gui.InitializeWorld(_world);
		_sceneData.Gui.OnGameModeChanged(_gameContext.ActiveMode);
	}

	private void CreateIdleSystems()
	{
		_idleSystems = new EcsSystems(_world);
		_postUpdateSystems = new EcsSystems(_world);

		_idleSystems
			.Add(new TimerTickSystem())
			.Add(new PlayerInputSystem())

			.Inject(_gameConfiguration)
			.Inject(_sceneData)
			.Inject(_gameContext)
			.Init();

		_postUpdateSystems
			.Add(new ChangeGameModeSystem())
			.Add(new EntityDestroySystem())

			.OneFrame<DestroyEntityRequest>()
			.OneFrame<ChangeGameModeEvent>()
			.Inject(_gameConfiguration)
			.Inject(_sceneData)
			.Inject(_gameContext)
			.Init();
	}

	private void CreateBattleSystems()
	{
		_battleUpdateSystems = new EcsSystems(_world);
		_battleFixedSystems = new EcsSystems(_world);

#if UNITY_EDITOR
		Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_battleUpdateSystems);
#endif

		_battleUpdateSystems
			.Add(new PlayerSystem())

			.Add(new AbilityClickSystem())
			.Add(new SummonAbilitySystem())
			.Add(new HealAbilitySystem())

			.Add(new WaveSystem())
			.Add(new MobSpawnSystem())
			.Add(new WeaponSwitchSystem())
			.Add(new BattleEndSystem())

			.Add(new BattleTargetSystem())
			.Add(new InAttackRangeSystem())

			.Add(new AnimationMovementSystem())

			.Add(new CanAttackSystem())
			.Add(new AttackCooldownSystem())
			.Add(new AttackAnimationSystem())
			.Add(new WeaponAttackSystem())

			.Add(new WeaponShootSystem())
			.Add(new BulletCollisionSystem())
			.Add(new BulletDestroySystem())
			.Add(new WeaponHitSystem())

			.Add(new HealSystem())
			.Add(new DamageOverTimeSystem())
			.Add(new TakeDamageSystem())

			.Add(new AbilityCooldownViewSystem())
			.Add(new HealthViewSystem())

			.Add(new NoHealthDieSystem())

			.OneFrame<UnitSpawnEvent>()
			.OneFrame<SpawnEvent>()
			.OneFrame<CanAttack>()
			.OneFrame<AttackAction>()
			.OneFrame<AnimationAttackCompleteEvent>()
			.OneFrame<MakeDamageRequest>()
			.OneFrame<HealRequest>()
			.OneFrame<HealthChangeEvent>()
			.OneFrame<WeaponShoot>()
			.OneFrame<ApplyAbility>()
			.OneFrame<AbilityClickEvent>()
			.OneFrame<TakeDamageAnimationEvent>()
			.OneFrame<AttackInput>()

			.Inject(_gameConfiguration)
			.Inject(_sceneData)
			.Inject(_blueprintFactory)
			.Inject(_gameContext)
			.Init();

		_battleFixedSystems
			.Add(new NavMeshMoveSystem())
			.Add(new DirectionMoveSystem())
			.Init();
	}

	private void Update()
	{
		_idleSystems?.Run();
		_battleUpdateSystems?.Run();
		_postUpdateSystems?.Run();
	}

	private void FixedUpdate()
	{
		_battleFixedSystems?.Run();
	}

	private void OnDestroy()
	{
		_battleUpdateSystems?.Destroy();
		_battleUpdateSystems = null;

		_battleUpdateSystems?.Destroy();
		_battleUpdateSystems = null;

		_idleSystems?.Destroy();
		_idleSystems = null;

		_world?.Destroy();
		_world = null;
	}

	#region GameModes

	public void OnModeLeave(GameActiveMode activeMode)
	{
		switch (activeMode)
		{
			case GameActiveMode.Battle:
				OnBattleModeLeave();
				break;
		}
	}

	public void OnModeEnter(GameActiveMode activeMode)
	{
		switch (activeMode)
		{
			case GameActiveMode.Battle:
				OnBattleModeEnter();
				break;
		}

		_sceneData.Gui.OnGameModeChanged(activeMode);
	}

	private void OnBattleModeLeave()
	{
		_battleUpdateSystems?.Destroy();
		_battleUpdateSystems = null;

		_battleUpdateSystems?.Destroy();
		_battleUpdateSystems = null;
	}

	private void OnBattleModeEnter()
	{
		_battleUpdateSystems?.Destroy();
		_battleUpdateSystems?.Destroy();
		CreateBattleSystems();
	}

	#endregion
}
