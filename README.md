# Ecs demo
## Gameplay idea
The player needs to survive waves of enemies. The player and enemies have weapons.
and the player has different abilities. The level is completed after defeating all enemies.

# ECS
The demo is written using the Entity Component System (ECS) architecture with [LeoEcs framework](https://github.com/Leopotam/ecs). The main entry point is Game.cs. There, all systems are initialized and dependencies are injected.
System contains a set of Filter<TComponent> and handles entities. e.g., a system that handles the movement of an entity in a desired direction:

```c#
public class DirectionMoveSystem : IEcsRunSystem
{
	private readonly EcsFilter<MovementDirection, MovementComponent> _directionMovement = null;

	public void Run()
	{
		foreach (int index in _directionMovement)
		{
			EcsEntity entity = _directionMovement.GetEntity(index);
			Vector3 direction = _directionMovement.Get1(index).Direction; 
			float speed = _directionMovement.Get2(index).Speed;

			entity.Get<Position>().Value += direction * speed * Time.deltaTime;
			Move(entity, entity.Get<Position>().Value, entity.Get<Rotation>().Value);
		}
	}

	private void Move(EcsEntity entity, Vector3 position, Quaternion rotation)
	{
		if (entity.Has<RigidbodyLink>())
		{
			entity.Get<RigidbodyLink>().Value.MovePosition(position);
			entity.Get<RigidbodyLink>().Value.MoveRotation(rotation);
		}
		else
		{
			entity.Get<GameObjectLink>().Value.transform.position = position;
			entity.Get<GameObjectLink>().Value.transform.rotation = rotation;
		}
	}
}
```
# Blueprints
Blueprint is a ScriptableObject that describes an entity prototype (components, settings, view that will be added to the entity when it is created by BlueprintFactory). In Project, there are UnitBlueprint, WeaponBlueprint, and BulletBlueprint.
e.g., BulletBlueprint. When an entity is created, the required components for bullet are added, such as <BulletTag>, <BattleOnly>, <MovementComponent>.
```c# 
public class BulletBlueprint : Blueprint
{
	[SerializeField] private MonoEntity _prefab;
	[SerializeField] private MovementComponent _bulletMoveComponent;

	public override EcsEntity CreateEntity(EcsWorld world)
	{
		EcsEntity entity = world.NewEntity();
		entity.Get<BulletTag>();
		entity.Get<BattleOnly>();
		entity.Get<MovementComponent>().Speed = _bulletMoveComponent.Speed;

		return entity;
	}

	public override Transform CreateView(EcsEntity entity, Transform parent)
	{
		MonoEntity view = _prefab.GetInstance(parent.transform);
		view.Make(ref entity);

		return view.transform;
	}
}
```
# Monolink and MonoEntity

It's a connection between MonoBehaviour Components and ECSWorld.
MonoLink adds an EcsComponent (that contains a link to MonoBehaviour) to the entity.
```c# 
public class MonoLink<T> : MonoLinkBase where T : struct
{
	public T Value;
    
	public override void Make(ref EcsEntity entity)
	{
		if (entity.Has<T>())
		{
			return;
		}

		entity.Get<T>() = Value;
	}
}
```
MonoEntity is added to the prefab and contains a collection of other Monolinks that were added to this prefab. When a MonoEntity is created, it binds all MonoLinks to the entity. It allows us to add MonoBehaviour components to a prefab, and automatically bind them to an entity.

RigitBodyMonoLink, for example, adds a link to the rigidbody component to Entity.
```c# 
[RequireComponent(typeof(Rigidbody))]
public class RigidBodyMonoLink : MonoLink<RigidbodyLink>
{
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (Value.Rigidbody == null)
        {
            Value = new RigidbodyLink
            {
                Rigidbody = GetComponent<Rigidbody>()
            };
        }
    }
#endif
}
```
